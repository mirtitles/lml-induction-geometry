\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {british}{}
\contentsline {chapter}{Preface}{v}
\contentsline {chapter}{Introduction: The method of mathematical induction}{1}
\contentsline {chapter}{\numberline {1}Calculation by Induction}{9}
\contentsline {chapter}{\numberline {2}Proof by Induction}{15}
\contentsline {chapter}{\numberline {3}Construction by Induction}{45}
\contentsline {chapter}{\numberline {4}Finding Loci by Induction}{53}
\contentsline {chapter}{\numberline {5}Definition by Induction}{59}
\contentsline {chapter}{\numberline {6}Induction on the Number of Dimensions}{71}
\contentsline {chapter}{Bibliography}{97}
